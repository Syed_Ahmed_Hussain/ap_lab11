; Syed Ahmed Hussain
; Class: BE(SE)-4B
; Reg.#: 05761
; Sub: Advanced Programming
; Lab 11: Scheme

(define aCompletelyUselessFunctionCreatedOnlyToMakeYourLifeMiserable
  (lambda (m n)
    (IFTHENELSE (OR (iszero m) (LT m n)) (SUM n z) (ADD m n))
  )
)

; Other functions / variables
	  
; variable z
(define z 0)

; if-then-else function
(define IFTHENELSE 
	(lambda (p a b)
		(p a b)
	)
)

;summation function
(define SUM
  (lambda (n a)
    (define (loop n a z)
      (if (zero? n)
        a
        (loop (pred 1 n) (ADD a z) (succ z 1))
      )
    )
    (loop n a z)
  )
)

; less than function
(define LT
  (lambda (a b)
    ( AND (iszero (SUB a b)) (NOT ( iszero (SUB b a) )) )
  )
)

; or function
(define OR
  (lambda (M N)
    (N true (M true false))
  )
)

; and function
(define AND
  (lambda (M N)
    (N (M true false) false)
  )
)

; not function
(define NOT
  (lambda (M)
    (M false true)
  )
)

; add function
(define ADD
  (lambda (a b)
    (succ a (succ b z))
  )
)

; subtract function
(define SUB
  (lambda (a b)
    (pred (succ b z) (succ a z) )
  )
)

; true function
(define true
  (lambda (a b)
    a
  )
)

; false function
(define false
  (lambda (a b)
    b
  )
)

; Functions defined in impure lambda calculus to test the above functions
; iszero function
(define iszero
  (lambda (a)
    (if(= a z)
    	true
    	false
    )
  )
)

; successor function
(define succ
  (lambda (a b)
    (+ a b)
  )
)

; predecessor function
(define pred
  (lambda (a b)
    (if (< (- b a) z)
       z
      (- b a)
    )
  )
)


